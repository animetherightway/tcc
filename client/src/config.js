const debug = false
const backutStatic = 'https://storage.googleapis.com/animetherightwaystatic/'
const basedir = debug ? 'http://192.168.149.130:8081/' : 'https://fabled-ruler-213319.appspot.com/'

const config = {
  ativo: 0,
  basedir,
  imageStatic: backutStatic + 'images/',
  noImage: backutStatic + 'images/no_image.jpg',
  dummy: new Date().getTime()
}

global.config = config

export default config
