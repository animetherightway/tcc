import Vue from 'vue'
import Router from 'vue-router'
import Animes from '@/components/Animes/Index'
import ViewAnimes from '@/components/ViewAnimes/Index'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/animes',
      name: 'animes',
      component: Animes
    },
    {
      path: '/animes/:id',
      name: 'anime',
      component: ViewAnimes
    },
    {
      path: '/animes/:id/:episodioId',
      name: 'anime_episodio',
      component: ViewAnimes
    },
    {
      path: '*',
      redirect: 'animes'
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    return {x: 0, y: 0}
  }
})
