import axios from 'axios'
import store from '@/store/store'

export default () => {
  return axios.create({
    baseURL: `${global.config.basedir}`,
    headers: {
      Authorization: `Bearer ${store.state.token}`
    }
  })
}
