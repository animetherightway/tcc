import Api from '@/services/Api'

export default {
  index (search, all = 0) {
    return Api().get('animes', {
      params: {
        search,
        all
      }
    })
  },
  show (id) {
    return Api().get(`animes/${id}`)
  }
}
