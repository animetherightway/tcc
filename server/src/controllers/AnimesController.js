const db = require('../models')

module.exports = {
  async index (req, res) {
    try {
      let animes = []
      const search = req.query.search || ''
      const all = req.query.all || 0
      let include = []
      if (all !== '1') {
        include.push({
          model: db.episodios,
          where: { hasVideo: 1 }
        })
      }
      if (search !== '') {
        animes = await db.animes.findAll({
          attributes: {
            exclude: ['descricao']
          },
          include,
          where: {
            $or: [
              'nome'
            ].map(key => ({
              [key]: {
                $like: `%${search}%`
              }
            }))
          },
          order: [['nome', 'ASC']]
        }).map(o => o.toJSON())
      } else {
        animes = await db.animes.findAll({
          attributes: {
            exclude: ['descricao']
          },
          limit: 40,
          include,
          order: [['nome', 'ASC']]
        }).map(o => o.toJSON())
      }

      for (let o of animes) {
        if (!o.image || o.image === ``) {
          o.image = global.config.noImage
        } else {
          o.image = `${global.config.imageStatic}${o.image}`
        }

        delete o.episodios
      }

      res.send(animes)
    } catch (err) {
      res.status(500).send({
        error: 'an error has occured trying to fetch the animes',
        err
      })
    }
  },
  async show (req, res) {
    try {
      let anime = await db.animes.findById(req.params.id, {
        include: [{
          model: db.episodios
        }]
      })

      anime = anime.toJSON()

      if (!anime.image || anime.image === ``) {
        anime.image = global.config.noImage
      } else {
        anime.image = global.config.imageStatic + anime.image
      }

      let path = `${global.config.videoPath}${anime.id}.mp4`
      anime.hasVideo = global.fileExists(path) ? 1 : 0
      anime.link = anime.hasVideo === 1 ? `${global.config.videoStatic}${anime.id}.mp4` : ``

      for (let o of anime.episodios) {
        if (!o.image || o.image === ``) {
          o.image = global.config.noImage
        } else {
          o.image = `${global.config.imageStatic}${anime.id}/${o.image}`
        }
        let path = `${global.config.videoPath}${anime.id}/${o.id}.mp4`
        o.hasVideo = global.fileExists(path) ? 1 : 0
        // `${global.config.basedir}animes/${id}/${o.id}/video`
        o.link = o.hasVideo === 1 ? `${global.config.videoStatic}${anime.id}/${o.id}.mp4` : ``
      }

      anime.episodios.sort((a, b) => {
        if (a.id < b.id) return 1
        if (a.id > b.id) return -1
        return 0
      })

      res.send(anime)
    } catch (err) {
      res.status(500).send({
        error: 'an error has occured trying to show the animes',
        err
      })
    }
  },
  async video (req, res) {
    try {
      const path = global.config.videoStatic + (req.params.episodioId ? `${req.params.id}/${req.params.episodioId}.mp4` : `${req.params.id}.mp4`)
      if (!global.fileExists(path)) {
        res.status(404).send({
          error: 'an error has occured trying to show the animes'
        })
      }
      const stat = global.fileMetadata(path)
      console.log('stat', stat)
      res.status(404).send({
        error: 'an error has occured trying to show the animes',
        stat
      })
      // const fileSize = stat.size
      // const range = req.headers.range
      // if (range) {
      //   const parts = range.replace(/bytes=/, '').split('-')
      //   const start = parseInt(parts[0], 10)
      //   const end = parts[1] ? parseInt(parts[1], 10) : fileSize - 1
      //   const chunksize = (end - start) + 1
      //   const file = global.fileCreateReadStream(path, {start, end})
      //   console.log('file2', file)
      //   const head = {
      //     'Content-Range': `bytes ${start}-${end}/${fileSize}`,
      //     'Accept-Ranges': 'bytes',
      //     'Content-Length': chunksize,
      //     'Content-Type': 'video/mp4'
      //   }
      //   res.writeHead(206, head)
      //   file.pipe(res)
      // } else {
      //   const head = {
      //     'Content-Length': fileSize,
      //     'Content-Type': 'video/mp4'
      //   }
      //   res.writeHead(200, head)
      //   const file = global.fileCreateReadStream(path)
      //   console.log('file1', file)
      //   file.pipe(res)
      // }
    } catch (err) {
      res.status(500).send({
        error: 'an error has occured trying to show the video',
        err
      })
    }
  }
}
