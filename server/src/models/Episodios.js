module.exports = (sequelize, DataTypes) => {
  const Episodios = sequelize.define('episodios', {
    nome: DataTypes.STRING,
    minutos: DataTypes.INTEGER,
    image: DataTypes.STRING,
    video: DataTypes.STRING,
    hasVideo: DataTypes.INTEGER
  })

  Episodios.associate = function (models) {
    Episodios.belongsTo(models.animes)
  }

  return Episodios
}
