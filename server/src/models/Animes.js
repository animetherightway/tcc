module.exports = (sequelize, DataTypes) => {
  const Animes = sequelize.define('animes', {
    nome: DataTypes.STRING,
    descricao: DataTypes.TEXT,
    image: DataTypes.STRING,
    data: DataTypes.DATE
  })

  Animes.associate = function (models) {
    Animes.hasMany(models.episodios)
  }

  return Animes
}
