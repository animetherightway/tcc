const fs = require('fs')

const backutStatic = 'https://storage.googleapis.com/animetherightwaystatic/'
const backutVideo = 'https://storage.googleapis.com/animetherightwayvideo/'

let bucket = null

if (process.env.GCLOUD_STORAGE_BUCKET && process.env.NODE_ENV === 'production') {
  const {Storage} = require('@google-cloud/storage')

  const storage = new Storage()

  bucket = storage.bucket(process.env.GCLOUD_STORAGE_BUCKET)
}

const fileExists = async function (path) {
  try {
    if (bucket) {
      return await bucket.file(path).exists()[0] ? 1 : 0
    } else {
      return fs.existsSync(path) ? 1 : 0
    }
  } catch (err) {
    return 0
  }
}

global.fileExists = fileExists

const fileMetadata = async function (path) {
  try {
    if (bucket) {
      const [metadata] = await bucket.file(path).getMetadata()
      return metadata
    } else {
      return fs.statSync(path)
    }
  } catch (err) {
    return {size: 0}
  }
}

global.fileMetadata = fileMetadata

const fileCreateReadStream = async function (path, obj = {}) {
  try {
    if (bucket) {
      return await bucket.file(path).createReadStream(obj)
    } else {
      return fs.createReadStream(path, obj) ? 1 : 0
    }
  } catch (err) {
    return {size: 0}
  }
}

global.fileCreateReadStream = fileCreateReadStream

const config = {
  ativo: 0,
  videoPath: 'videos/',
  videoStatic: backutVideo + 'videos/',
  imageStatic: backutStatic + 'images/',
  noImage: backutStatic + 'images/no_image.jpg'
}

global.config = config

module.exports = {
  port: process.env.PORT || 8081,
  db: {
    user: process.env.SQL_USER || 'root',
    password: process.env.SQL_PASSWORD || '58484614',
    database: process.env.SQL_DATABASE || 'animetherightway',
    options: {
      dialect: process.env.SQL_DIALECT || 'mysql',
      host: process.env.HOST || 'localhost',
      charset: 'utf8'
    }
  },
  authentication: {
    jwtSecret: process.env.JWT_SECRET || 'secret'
  }
}
