const AnimesController = require('./controllers/AnimesController')

module.exports = (app) => {
  app.get('/animes',
    AnimesController.index)
  app.get('/animes/:id',
    AnimesController.show)
  app.get('/animes/:id/video',
    AnimesController.video)
  app.get('/animes/:id/:episodioId/video',
    AnimesController.video)
}
