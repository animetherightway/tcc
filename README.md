# Anime para todos
Anime para todos é o projeto que ira trazer todos os animes do japão para você

# Setup

Você precisa do Node Js instalado: https://nodejs.org/en/

Você precisa de uma versão igual ou maior que **NODE version 8.2.1**

Você não sabe instalar diferentes versões do node?  Tente isto: https://github.com/tj/n

Eu sugiro que você instale esses aplicativos:

VSCode: https://code.visualstudio.com/

ITerm2: https://www.iterm2.com/ (Mac Only)

Git: https://git-scm.com/

### Client - Terminal A
```
cd client
npm install
npm start
```

### Server - Terminal B
```
cd server
npm install
npm start
```
